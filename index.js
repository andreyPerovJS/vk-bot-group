const mongoose = require('mongoose');
const request = require('request-promise');
const fs = require('fs');
const rimraf = require('rimraf');
const cron = require('node-cron');
const { isEmpty } = require('lodash');

const { config } = require('./db');

const Parser = require('./app/parser');

// Consts
const { productsUrls } = require('./app/consts/urls');
const parser = require('./app/consts/rules');

// Services
const { MongoServices } = require('./app/services/mongo-service');
const { VkService } = require('./app/services/vk');

// Utils
const { logger } = require('./app/modules/logger');
const { download, createPartnerLink } = require('./app/utils').utils;

mongoose.connect(config, {useNewUrlParser: true});

const isParser = process.argv[2];

const db = mongoose.connection;

db.on('error', err => console.log(`error - ${err}`))
db.once('open', async () => {
  if (isParser) {
    new Parser(productsUrls, parser).grabbing();

    return
  }

  try {
    cron.schedule('*/10 * * * *', async () => {
      const product = await MongoServices.getOneProduct()
      const destination = await VkService.getWallUploadServer(7621608)

      let images = product.images

      if (isEmpty(images)) {
        await download({
          type: 'relative',
          target: product.original,
          path: image,
          pathTo: './tmp_files'
        })
      }

      for (let k = 0; k < images.length; k++) {
        let image = images[k].slice(2)

        image && await download({
          type: 'absolute',
          target: product.original,
          path: image,
          pathTo: './tmp_files'
        })
      }

      const mapToFormData = () => {
        return fs.readdirSync(`${__dirname}/tmp_files/`)
          .reduce((acc, file, index) => ({
            ...acc,
            [`file${index + 1}`]: fs.createReadStream(`${__dirname}/tmp_files/${file}`)
          }), {})
      }
    
      const response = await request.post({
        url: destination.upload_url,
        headers: {'content-type' : 'multipart/form-data'},
        formData: {
          filename: 'myfile.jpg',
          ...mapToFormData()
        }
      });
    
      const res = JSON.parse(response)
    
      const uploadedPhoto = await VkService.saveWallPhoto({
        album_id: 14,
        server: res.server,
        photo: res.photo,
        hash: res.hash,
      })
    
      const mapToUploadedPhoto = uploadedPhoto.reduce((acc, photo, index) => {
        acc += `photo${uploadedPhoto[index].owner_id}_${uploadedPhoto[index].id},`
        return acc
      }, ``)
    
      const shortLink = await createPartnerLink(product.href)
    
      const postWall = await VkService.postWall({
        message: `
          ${product.brandAndType}
          ${product.description}
          Старая цена: ${product.oldPrice} руб.
          Текущая цена: ${product.currentPrice} руб. ${shortLink.short_url}
          Скидка: ${product.sale}%
          Размеры в наличии: ${product.sizes}
          ${shortLink.short_url}
        `,
        owner_id: -7621608,
        from_group: 1,
        attachments: mapToUploadedPhoto
      })
    
      logger.info('Posted wall')

      const removeID = await MongoServices.removeProduct(product._id)

      logger.info(`Deleted object by ID: ${removeID._id}`)
    
      rimraf(`${__dirname}/tmp_files/*`, {}, () => {})
    })
  } catch (error) {
    console.log(error)
  }
})
