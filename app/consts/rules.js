const uuid = require('uuid/v1');
const request = require('request-promise');
const { random, isEmpty } = require('lodash');

const { setOptions } = require('../utils').utils;
const { logger } = require('../modules/logger');

const ProductSchema = require('../models/products');

module.exports = {
  lamoda: {
    url: 'https://www.lamoda.ru',
    sale: 25,
    getStateResultParser: async function queue($, options) {
      const data = [];
      const { urlRequest } = options;

      for (let i = 0; i < $('.products-list-item').length; i++) {
        let $item = $('.products-list-item').eq(i);
        
        let sale = $item.find('.product-label_cat').data('discount');

        console.log(sale);

        if(sale >= this.sale) {
          const product = { id: uuid(), images: [] };

          product.sizes = $item.find('.products-list-item__sizes').text();
          product.brandAndType = $item.find('.products-list-item__type').text().slice(3);
          product.priceOld = $item.find('.price__old').text();
          product.priceNew = $item.find('.price__new').text();
          product.image = $item.data('src');
          product.href = $item.find('a').attr('href');

          let productDone = await new Promise(resolve => {
            request(setOptions(`${this.url + product.href}`)).then($ => {
              logger.info(`Выполняется запрос - ${this.url + product.href}`);

              $('.showcase__slide').each((index, item) => {
                const img = $(item).data('fullsize');

                product.images.push(img);
              })

              const record = new ProductSchema({
                brandAndType: product.brandAndType,
                currentPrice: product.priceNew,
                oldPrice: product.priceOld,
                sizes: product.sizes,
                original: this.url,
                sale,
                image: product.image,
                images: product.images,
                href: product.href
              })

              !isEmpty(product.images) && record.save(console.log)

              return setTimeout(() => resolve(product), random(7000, 17000))
            })
            .catch(console.log)
          })

          logger.info(`Продукт ${product.brandAndType} получен`)

          data.push(productDone);
        }
      }

      return data;
    }
  },
  superstep: {
    url: 'https://superstep.ru',
    sale: 25,
    getStateResultParser: async function($) {
      const data = [];

      for (let i = 0; i < $('.product_item').length; i++) {
        const $item = $('.product_item').eq(i);
        const sale = $item.find('.c-label--tag .c-label__text').text();

        if(sale.slice(0, 2) >= this.sale) {
          const product = { id: uuid() };

          product.name = $item.find('.name a').attr('title');
          product.brandAndType = $item.find('.brand a').text();
          product.priceOld = $item.find('.old_price').text();
          product.priceNew = $item.find('.new_price').text();
          product.href = $item.find('.thumbnail a').attr('href');
          product.image = $item.find('.thumbnail img').attr('src');

          let productDone = await new Promise(resolve => {
            request(setOptions(`${this.url + product.href}`)).then($ => {
              logger.info(`Выполняется запрос - ${this.url + product.href}`);

              product.description = $('.descript_bl').find('.ss').text();

              const record = new ProductSchema({
                brandAndType: product.brandAndType,
                currentPrice: product.priceNew,
                oldPrice: product.priceOld,
                sizes: product.sizes,
                sale,
                original: this.url,
                description: product.description,
                image: product.image,
                href: product.href
              })
              
              record.save(console.log)

              return setTimeout(() => resolve(product), random(7000, 17000))
            })
            .catch(console.log)
          })

          logger.info(`Продукт ${product.name} получен`)

          data.push(productDone)
        }
      }

      return data;
    }
  }
};