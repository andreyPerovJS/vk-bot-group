module.exports = {
  productsUrls: {
    lamoda: [
      // 'https://www.lamoda.ru/b/25889/brand-thenorthface/?is_sale=1',
      // 'https://www.lamoda.ru/b/4869/brand-calvinkleinjeans/?order=new&sitelink=calvinkleinjeanssis&l=1p1&is_sale=1',
      // 'https://www.lamoda.ru/b/573/brand-tommyhilfiger/?is_sale=1',
      // 'https://www.lamoda.ru/b/23682/brand-tommyhilfigerdenim/?is_sale=1',
      // 'https://www.lamoda.ru/b/25571/brand-underarmour/?is_sale=1', 
      // 'https://www.lamoda.ru/b/1061/brand-adidas/?is_sale=1', 
      // 'https://www.lamoda.ru/b/1163/brand-adidasoriginals/?is_sale=1',
      // 'https://www.lamoda.ru/b/3753/brand-bensherman/?is_sale=1',
      // 'https://www.lamoda.ru/b/6158/brand-fredperry/?is_sale=1',
      // 'https://www.lamoda.ru/b/25138/brand-gap/?is_sale=1',
      // 'https://www.lamoda.ru/b/22293/brand-hellyhansen/?is_sale=1',
      // 'https://www.lamoda.ru/b/471/brand-lacoste/?is_sale=1',
      // 'https://www.lamoda.ru/b/5615/brand-lecoqsportif/?is_sale=1',
      // 'https://www.lamoda.ru/b/4361/brand-levis/?is_sale=1',
      // 'https://www.lamoda.ru/b/6156/brand-merc/?is_sale=1',
      // 'https://www.lamoda.ru/b/5816/brand-newbalance/?is_sale=1',
      // 'https://www.lamoda.ru/b/2047/brand-nike/?is_sale=1',
      // 'https://www.lamoda.ru/b/1063/brand-reebok/?is_sale=1', 
      // 'https://www.lamoda.ru/b/18583/brand-reebokclassics/?is_sale=1'
    ],
    superstep: [
      'https://superstep.ru/catalog/muzhskoe/filter/code_brand-adidas/?PAGEN_1=3',
      'https://superstep.ru/brand/nb/',
      'https://superstep.ru/brand/lacoste/',
      'https://superstep.ru/brand/diadora/', 
      'https://superstep.ru/brand/reebok/',
      'https://superstep.ru/brand/asics/', 
      // 'https://superstep.ru/brand/nike/',
      // 'https://superstep.ru/brand/tommyhilfiger/',
      // 'https://superstep.ru/brand/converse/',
      // 'https://superstep.ru/brand/hugo_boss/',
      // 'https://superstep.ru/brand/fila/',
      // 'https://superstep.ru/brand/puma/',
      // 'https://superstep.ru/brand/polo_rl/',
      // 'https://superstep.ru/brand/fred_perry/',
      // 'https://superstep.ru/brand/lumberjack/',
      // 'https://superstep.ru/brand/northface/', 
      // 'https://superstep.ru/brand/wrangler/'

    ] 
  }
}