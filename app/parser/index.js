const cheerio = require('cheerio');
const request = require('request-promise');
const fs = require('fs');
const { flattenDepth } = require('lodash');

const { recordFile } = require('../modules/csv');
const { productsUrls } = require('../consts/urls');
const parser = require('../consts/rules');
const { logger } = require('../modules/logger');

class Parser {
  constructor(parseUrls = null, searchRules) {
    this.parseUrls = parseUrls;
    this.searchRules = searchRules;
    this.matches = [];
  }

  async sendRequest(url, parser, matches) {
    const options = {
      uri: url,
      transform: body => cheerio.load(body)
    }

    return request(options)
      .then($ => parser.getStateResultParser($, { url }))
      .then(data => matches.push(data) && flattenDepth(matches, 2))
      .then(prepareData => recordFile(prepareData))
      .then(csv => {
        fs.writeFile('file.csv', csv, function(err) {
          if (err) throw err;
          console.log('file saved');
          logger.info(`Запрос страницы выполнен - ${url}`);
        });
      })
      .catch(console.log)
  }

  async grabbing() {
    const { parseUrls, sendRequest, searchRules, matches } = this;

    if(!parseUrls) {
      throw new Error("Not found not one a link");
    }

    const mainKeysUrls = Object.keys(parseUrls);

    for(let i = 0; i < mainKeysUrls.length; i++) {
      const childUrls = parseUrls[mainKeysUrls[i]];

      for(let j = 0; j < childUrls.length; j++) {
        logger.info(`Запрос страницы - ${childUrls[j]}`);
        await sendRequest(childUrls[j], searchRules[mainKeysUrls[i]], matches)
      }
    }
  }
}

module.exports = Parser;
