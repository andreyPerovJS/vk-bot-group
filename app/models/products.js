const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  brandAndType: String,
  currentPrice: String,
  original: String,
  oldPrice: String,
  sizes: String,
  sale: String,
  image: String,
  images: [String],
  href: String,
  description: {
    type: String,
    default: ''
  }
})

module.exports = mongoose.model('Products', ProductSchema)