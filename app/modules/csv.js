const Json2csvParser = require('json2csv').Parser;

const fields = 
      [{label: 'id', value: 'id'},
      {label: 'images', value: 'images'},
      {label: 'sizes', value: 'sizes'},
      {label: 'brandAndType', value: 'brandAndType'},
      {label: 'priceOld', value: 'priceOld'},
      {label: 'priceNew', value: 'priceNew'},
      {label: 'image', value: 'image'},
      {label: 'href', value: 'href'}]

const json2csvParser = new Json2csvParser({fields});


module.exports = {
  recordFile: data => json2csvParser.parse(data)
}