const winston = require('winston');
const moment = require('moment');

const { combine, timestamp, label, printf, colorize, prettyPrint } = winston.format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = winston.createLogger({
  level: 'info',
  format: combine(
    label({ label: 'Запрос к серверу магазина' }),
    timestamp({
      format: () => {
        return moment().format("YYYY-MM-DD HH:mm:ss");
      }
    }),
    prettyPrint(),
    colorize(),
    myFormat
  ),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.Console({
      format: winston.format.simple()
    }),
    new winston.transports.Console({ level: 'error' }),
  ]
});

module.exports = {
  logger
}