const fs = require('fs');
const cheerio = require('cheerio');
const request = require('request');
const uuidv4 = require('uuid/v4');
const { VkService } = require('./services/vk')

const setOptions = url => ({
  uri: url,
  transform: body => cheerio.load(body)
})

const download = (args) => {
  const { pathTo, type, target } = args
  const path = type === 'absolute' ? `https://${args.path}` : target + path

  return new Promise((resolve, reject) => {
    request
      .get(path)
      .pipe(fs.createWriteStream(`${pathTo}/${uuidv4()}.png`))
      .on('finish', () => resolve());
  })
};

const createPartnerLink = url => {
  return new Promise((resolve, reject) => {
    if (url) {
      const hash = '3f2779c2d4de2c46eded4e8640d77b'
      const response = VkService.getShortLink(`https://modato.ru/g/${hash}/?ulp=https://www.lamoda.ru${url}`)
      resolve(response)
    }
  })
}

module.exports = {
  utils: {
    setOptions,
    download,
    createPartnerLink
  }
}