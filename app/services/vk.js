require('dotenv').config();
const { VK } = require('vk-io');
 
const vk = new VK();
 
vk.token = process.env.TOKEN;

const VkService = {
  postWall: data => new Promise(resolve => {
    const response = vk.api.wall.post(data)
    setTimeout(() => resolve(response), 7000)
  }),
  getWallUploadServer: id => vk.api.photos.getWallUploadServer(id),
  saveWallPhoto: data => vk.api.photos.saveWallPhoto(data),
  getShortLink: url => vk.api.utils.getShortLink({ url })
}

module.exports = {
  VkService
}
