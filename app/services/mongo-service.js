const ProductSchema = require('../models/products');

const MongoServices = {
  getProducts: () => new Promise((resolve, reject) => {
    ProductSchema.find((err, products) => {
      err ? reject(err) : resolve(products)
    })
  }),
  getOneProduct: () => new Promise((resolve, reject) => {
    ProductSchema.findOne((err, product) => {
      err ? reject(err) : resolve(product)
    })
  }),
  removeProduct: (id) => new Promise((resolve, reject) => {
    ProductSchema.findOneAndRemove(id, (err, response) => {
      err ? reject(err) : resolve(response)
    })
  })
}

module.exports = {
  MongoServices
}