require('dotenv').config();

const { USER_DB, PASS } = process.env;

module.exports = {
  config: `mongodb://${USER_DB}:${PASS}@ds137404.mlab.com:37404/todo-box`
}